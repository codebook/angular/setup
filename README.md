# Getting Started with New Angular

_tbd_



## References

* [Angular](https://angular.io/)
* [Angular CLI](https://cli.angular.io/)
* [Angular Material](https://material.angular.io/)
* [Angular Universal](https://github.com/angular/universal)
* [angular-ui/ui-router](https://github.com/angular-ui/ui-router)
* [RxJS](http://reactivex.io/rxjs/)
* [ngrx/platform](https://github.com/ngrx/platform)
* []()
* [aspnetcore-angular2-universal](https://github.com/MarkPieszak/aspnetcore-angular2-universal)
* [Running Serverless ASP.NET Core Web APIs with Amazon Lambda](https://aws.amazon.com/blogs/developer/running-serverless-asp-net-core-web-apis-with-amazon-lambda/)
* [Angular Universal and Server Side Rendering Step-By-Step](https://malcoded.com/posts/angular-fundamentals-universal-server-side-rendering)
* [Angular 4 with server side rendering (aka Angular Universal)](https://medium.com/burak-tasci/angular-4-with-server-side-rendering-aka-angular-universal-f6c228ded8b0)
* [Angular Universal Integration](https://github.com/angular/angular-cli/wiki/stories-universal-rendering)
* []()
* []()

